(require 'package)
(package-initialize)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)

(package-install 'htmlize)
(package-install 'json-mode)
(package-install 'org)
(package-install 'weblorg)
(package-install 'yaml-mode)
