# renovate: datasource=docker depName=nginx
ARG NGINX_VERSION=1.21.6

FROM node:lts-alpine as css
WORKDIR /garbage

RUN npm install -g sass
COPY sass sass
RUN mkdir -p output/stylesheets
RUN sass sass:output/stylesheets

FROM nginx:$NGINX_VERSION-alpine
COPY output/the-library-genesis-api.html /usr/share/nginx/html/
COPY --from=css /garbage/output/stylesheets /usr/share/nginx/html/stylesheets

COPY index.html /usr/share/nginx/html/
COPY gifs /usr/share/nginx/html/gifs
