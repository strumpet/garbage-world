(require 'package)
(package-initialize)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(require 'org)
(require 'weblorg)

(require 'htmlize)
(require 'json-mode)
(require 'yaml-mode)
(setq org-html-htmlize-output-type 'css)

(weblorg-site
 :template-vars '(("site_name" . "GARBAGE WORLD")
                  ("site_description" . "stop touching the computer")))
(setq weblorg-default-url (getenv "ROOT_URL"))

(weblorg-route
 :name "posts"
 :input-pattern "posts/*.org"
 :template "post.html"
 :output "output/{{ slug }}.html"
 :url "{{ slug }}.html")

(weblorg-route
 :name "index"
 :input-pattern "posts/*.org"
 :input-aggregate #'weblorg-input-aggregate-all-desc
 :template "blog.html"
 :output "output/index.html"
 :url "/")

(weblorg-route
 :name "feed"
 :input-pattern "posts/*.org"
 :input-aggregate #'weblorg-input-aggregate-all-desc
 :template "feed.xml"
 :output "output/feed.xml"
 :url "/feed.xml")

(weblorg-export)
